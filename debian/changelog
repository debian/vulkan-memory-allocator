vulkan-memory-allocator (3.2.1+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Matthias Geiger <werdahias@debian.org>  Sun, 09 Feb 2025 17:13:27 +0100

vulkan-memory-allocator (3.2.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * d/control: Update my mail address
  * d/control: Bump S-V to 4.7.0; no changes needed
  * d/u/metadata: Update for new upstream url
  * Install VulkanMemoryAllocatorConfigVersion.cmake, too

 -- Matthias Geiger <werdahias@debian.org>  Fri, 03 Jan 2025 19:14:31 +0100

vulkan-memory-allocator (3.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Updated gbp.conf for switch to regular releases
  * Updated d/watch to track tags instead of snapshots

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 31 May 2024 13:29:30 +0200

vulkan-memory-allocator (3.0.1+git20230911+2f382df+dfsg-2) unstable; urgency=medium

  * Marked vulkan-memory-allocator-doc as MA: foreign
  * Marked libvulkan-memory-allocator-dev as Arch: all and Multi-Arch:
    foreign

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 26 Oct 2023 15:44:15 +0200

vulkan-memory-allocator (3.0.1+git20230911+2f382df+dfsg-1) unstable; urgency=medium

  * Switch to upstream git snapshot (Closes: #1053350)
  * Updated my mail address in d/control and d/copyright
  * Upload to unstable (Closes: #1051406)
  * Bumped minimum cmake version in d/control
  * Set VMA_BUILD_DOCUMENTATION flag in d/rules to build docs with git
    snapshot
  * Install documentation with a separate .install file
  * Exclude prebuilt docs from source tarball, build it from source and drop
    lintian overrides for it
  * Include patch to exclude build path from the documentation files
  * Add Section: doc for vulkan-memory-allocator-doc in d/control

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 24 Oct 2023 20:45:55 +0200

vulkan-memory-allocator (3.0.1+dfsg-1) experimental; urgency=medium

  * Initial release. (Closes: #1035727) 

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Mon, 08 May 2023 14:11:04 +0200
